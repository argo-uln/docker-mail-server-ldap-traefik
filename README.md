# An Email server.

> Docker mail server is a full-featured mail server constucted of various Docker images. The goal of the project is to be an easily configurable and long-term maintainable and fully functional mail server. An exclusive feature is an automatically created and updated Address Book implemented using `OpenLDAP`.

Main features:

- **Receiving mail** for all of Your virtual domains.
- **Filtration** of spam and viruses.
- **Dispatch** of Your mail.
- **Encryption** of all connections (if possible).
- Usage of **DKIM signatures**.
- Server disk-based **Email storage**.
- Utilization of **quota-based** strategy (size limitation of mailboxes).
- Letting users 
	- to control **filtration** of mail
	- to organize mail in folders
	- to forward mail
	- to have an automated reply option if they are on leave
- Access via **Webmail**
 - `PostfixAdmin` is used to administer mailboxes and domains.
 - Automatically updated Address Book that utilizes `OpenLDAP`(It is automatically updated when users appear and are deleted in the `PostfixAdmin` database. The mail alias named **All domain users** is created automatically in LDAP and in `PostfixAdmin` database.)

## How does it work?
-----------------
**Let’s take a look at an example of user sending email.**
![User sends an email to an email server on the internet](https://gitlab.com/argo-uln/docker-mail-server-ldap-traefik/-/raw/main/img/submission.png)
1. User is securely connected to Dovecot on submission port 587(STARTTLS). 
2. Dovecot ensures the user is genuine by checking login&password pair from the database with those provided by user connection. 
3. If the user is genuine email is forwarded to Postfix.
4. Postfix sends the email to RSPAMD in order to check whether it looks like spam mail.
5. RSPAMD stores data in redis.
6. RSPAMD checks the email for viruses (on virus found email halts here).
7. After RSPAMD checks are successful email is returned to Postfix.
8. Postfix sends the email to the addressee.

**Receiving mail from somebody on the internet.**
![Receiving mail from the outside](https://gitlab.com/argo-uln/docker-mail-server-ldap-traefik/-/raw/main/img/mail-in.png)

1. Remote email server tries to connect to our Postfix server using SMTP.
2. Postfix checks for addressee domain in the database. Upon a match in some domain name Postfix checks whether addressee username exists.
3. If a match in addressee username@domain-name is found email is sent to RSPAMD in order to check whether it looks like spam mail.
4. RSPAMD stores data in redis.
5. RSPAMD checks the email for viruses (on virus found email halts here).
6. After RSPAMD checks successful email is returned to Postfix.
7. Postfix sends the email to Dovecot for it to be locally put into user email directory.
8. Dovecot looks up Sieve filters and stores user email locally according to them.

## Preconfiguration.

Before proceeding with installation it is mandatory to configure DNS for at least a single domain name.

**Hostname of the Email server.**

Hostname must be unique. It is a complete name of the domain and it designates the IP address of Your server.
For example, if You were to name the Email domain with ```example.org```, your Email hostname of the server would be ```mail.example.org``` respectively.
Double-check whether there is a DNS lookup for the hostname 

```mail.example.org. IN A x.x.x.x```

where ```x.x.x.x``` would be the IP address of an outbound interface of Your server. An exception would be a DMZ server. In this case what You want to be using is port forwarding from the public IP to the designated server.

**MX record**

Every single Email domain must have an MX record in DNS. It is used by other Email servers in order to establish your hostname. After that hostname is resolved to an IP with a record A in DNS. 
For a domain named ```example.org``` MX record would look like  

```example.org. IN MX 10 mail.example.org.```

For another domain named ```mydomen1.com``` MX record would look like 

```mydomen1.com IN MX 10 mail.example.org.```

**Record in a reverse DNS lookup.**

It is highly recommended to configure a reverse DNS lookup record. For a DNS name like ```mail.example.org``` resolved to an IP of ```x.x.x.x```, the IP ```x.x.x.x``` must resolve to ```mail.example.org```

This condition can be checked with a simple ```nslookup x.x.x.x```

Reverse DNS lookup record can be changed by the owner of the IP address. Usually that is Your ISP. If reverse DNS lookup is not configured it may lead to mail sent from Your Email server deemed as SPAM by most Email servers throughout the Internet.

**DKIM, SPF, DMARC records.**

DKIM domain record is created during the installation process. It can be accessed with ```cat filter/dkim/mail.pub```

```mail._domainkey IN TXT "v=DKIM1; k=rsa; ""p=MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA72iwPIqgo7Q91GEgNPafzwavpIYW0pm5Lm6T7Z65AcRS6R/acAZUaCLJiqZp8LpUwiUnjsCo0r2rOwMEGO83BQoe3kXdGUdgnngOYvS+AiQP5QOd9vZfYkhgAMrR9KAdl9dMaq1cd9PkxV4aQRwu2xiuXJfDZIMtSCn/25MaaiCUROw7VZmi/J/As/z1ya1lfXGS6Iy2yC0zbrO8G""wMoeM4YqyabONDJx4CpkynQ7WOexu27SClalNv0tIme7xSyKK2KDBf4ZmbUyWaZzz4/dDKUFJs3JAolFFoo/JGr7rI7d/JSO0ruUJyPM2pwv0NcO6E+0D28VVbAnWK3X7uQMQIDAQAB"```

SPF records look like this 

```@ IN TXT "v=spf1 a mx ip4:x.x.x.x ~all"```

DMARC records look like this 

```_dmarc IN TXT "v=DMARC1;p=none"```

## Installation

1. Run ```git clone https://gitlab.com/argo-uln/docker-mail-server-ldap-traefik.git.git```
2. Open the ```cd docker-mail-server``` directory

***Take notice:*** Correct the value of the `$alias_cn` variable from Russian to the language of Your preference in the `docker-mail-server-ldap-traefik/openldap/conf/mysql_ldap.php.dist`. In English that would be "All domain users".

3. Run the preparation script ```./setup.sh``` See what this script does below in the section [Installation Comments](https://gitlab.com/argo-uln/docker-mail-server-ldap-traefik#installation-comments).
4. When asked ```Please enter MYSQL PASSWORD:``` (enter desired mysql password)
   The same goes for the rest
5. ```Please enter FQDN hostname:``` (enter the complete hostname, e.g. mail.example.org)
6. ```Please enter mail domain:``` (enter the domain name, e.g. example.org)
7. ```Please enter RSPAMD web access password:``` (enter desired password used to access RSPAMD web-interface)
8. ```Please enter LDAP user admin password:``` (enter desired admin account password used for Openldap)
9. `Please enter LDAP user adressbook password:` (enter desired LDAP account `addressbook` password - He has access to read the address book.)
10. ```Please enter the ip addresses separated by a space to access the services postfixadmin, rspamd. Default, access to them is closed:``` (enter IP addresses separated with a whitespace which shall gain access for the postfixadmin, rspamd, phpldapadmin services)
11. ```You can start the build and run with the command: docker-compose up -d``` Everything is ready to be run with the appropriate command
12. Run ```docker-compose up``` and wait for the build and run processes to finish’ completion.
13. Open https://mail.example.org/postfixadmin/setup.php (mail.example.org shall be changed to your server name)
14. Enter the mysql password.
![Enter the mysql password](https://gitlab.com/argo-uln/docker-mail-server-ldap-traefik/-/raw/main/img/setup_password.png) 
15. Scroll down and enter the mysql password once again. Create a postfixadmin account. There are several password composition restrictions. It should contain at least 5 symbols (3 letters at minimum as well as 2 numbers and a single special symbol). User name should be along the lines of admin@example.org (example.org shall be changed to Your domain name).
![Create a postfixadmin account](https://gitlab.com/argo-uln/docker-mail-server-ldap-traefik/-/raw/main/img/add_superadmin_acc.png)
16. Open https://mail.example.org/postfixadmin/login.php.
17. Create a new email domain e.g. example.org. Create an admin account admin@example.org. There are several password composition restrictions. It should contain at least 5 symbols (3 letters at minimum as well as 2 numbers and a single special symbol).
18. Open the web interface of the created domain https://mail.example.org/mail/. Log in as admin@example.org. 
19. Assign a **dkim** record in dns zones of every of Your domains. To get a key run ```cat filter/dkim/mail.pub```

```
mail._domainkey IN TXT ( "v=DKIM1; k=rsa; "
	"p=ваш-dkim-ключ" ) ;
```
20. You can ensure it is eligible at https://dkimcore.org/tools/keycheck.html

* Field ```Selector:``` is always mail
* Field ```Domain name:``` should be filled with example.org
* Click - ```Check```

***Take notice:*** A single key is used for every of Your domains.

21. For ```RSPAMD``` access open https://mail.example.org/rspamd/
Enter the RSPAMD password. 
22. Open ```Сonfiguration```.
23. Settings should be set to these

* ```Greylist```        4
* ```Probably Spam```   6
* ```Rewrite subject``` 7
* ```Spam```            150 (mail tagged with “Spam” is guaranteed to pass and will be put into the “Spam” folder)

24. Save the changes. ```Save action```, ```Save cluster```. 

***Please note:*** If a virus is detected in the email, it will be discarded.

25. Test your emails for SPAM on [https://www.mail-tester.com/](https://www.mail-tester.com/)

## Installation Comments

**Let's see what `setup.sh` does.**
 
- During the work routine of ```setup.sh``` ```php-cli``` package will be installed (if not installed prior).
- A folder named ```/mail``` will be created in the root folder. It stores all mailboxes, folder access level is 777.
- An environment file named ```.env``` will be created using this template file: ```.env.dist```
- ```nginx/service.conf``` is created from the according template file ```nginx/service.conf.dist```. IP addressed with access to the ```PostfixAdmin```, ```Rspamd``` services (set during the setup.sh script execution) will be appended inside the file.
- `openldap/ldif/addressbook.ldif.dist` is a template used to create the `openldap/ldif/addressbook.ldif` configuration file.
- `openldap/ldif/ldap-add-ou-addressbook.ldif.dist` is a template used to create the `openldap/ldif/ldap-add-ou-addressbook.ldif` configuration file.
- `openldap/ldif/oclAccess.ldif.dist` is a template used to create the `openldap/ldif/oclAccess.ldif` configuration file.
 - ```openldap/conf/mysql_ldap.php.dist``` template is used to construct the ```openldap/conf/mysql_ldap.php``` file. It would be automatically executed on an hourly basis. It reads ```Mysql``` domain record and users. Creates a new ```Openldap``` address book entity, an alias ```To every user of the domain```. In ```Mysql``` an alias ```To every user of the domain``` is created meanwhile. ```Openlpad``` address book is updated upon user deletion. ```Mysql``` alias ```To every user of the domain``` is updated accordingly.
- ```roundcube/config/ldap.php.dist``` is a template used to create the ```roundcube/config/ldap.php``` configuration file.
- This file is a template ```roundcube/config/managesieve.php.dist``` used to create roundcube configuration file ```roundcube/config/managesieve.php```
- This file is a template ```postfixadmin/config.local.php.dist``` used to create postfixadmin configuration file ```postfixadmin/config.local.php```.
- This file is a template ```init/roundcube.sql.dist``` used to create file ```init/roundcube.sql```.

## Environment variables.

After running the preparation script ```./setup.sh``` You will shortly be met with a request to set some variables:
`MYSQL PASSWORD`, `FQDN hostname`, `mail domain`, `RSPAMD web access password`, `LDAP user admin password`, `LDAP user adressbook password`, `ip address`.

- Template `.env.dist` is used to create the `.env` file which contain environment variables
- `MYSQL PASSWORD` is put into `POSTFIXADMIN_DB_PASSWORD`, `PA_SETUP_PASS`, `MYSQL_ROOT_PASSWORD`, `MYSQL_PASSWORD`, `ROUNDCUBEMAIL_DB_PASSWORD` of the `.env` file. `MYSQL PASSWORD` is also used to generate `PA_SETUP_HASH` variable for the `postfixadmin/config.local.php` file
- `FQDN hostname` value is put into `MAILNAME`, `LE_FQDN`, `ROUNDCUBEMAIL_SMTP_SERVER`, `ROUNDCUBEMAIL_DEFAULT_HOST` of the `.env` file. `FQDN hostname` is also to be used in `roundcube/config/managesieve.php`. `FQDN hostname` is used to generate LDAP variables `BASEDN` and `ROOTDN`. They are used in the `openldap/conf/mysql_ldap.php` file which creates and updates the `Address book` and `To every user of the domain` alias in `mysql`
- Value of `mail domain` is put into `DOMAIN`, `POSTMASTER`, `LE_EMAIL` of the `.env` file and is used in the `postfixadmin/config.local.php` file
- `RSPAMD web access password` is put into `CONTROLLER_PASSWORD` of the `.env` file
- `LDAP user admin password` is put into `LDAP_PASS` of the `.env` file and is also used in the `openldap/conf/mysql_ldap.php` and `roundcube/config/ldap.php` files
- `LDAP user adressbook password` is put into `ADDRESSBOOK_PASS` of the `.env` file, and is also used in the `openldap/ldif/addressbook.ldif` file
- `ip address` is put into the `nginx/service.conf` file and is also used to give this(ose) IP(s) address(es) access to Postfixadmin and RSPAMD services

## Description of variables of the `.env.dist` template file

| Variable | Description |
| ------ | ------ |
| POSTFIXADMIN_DB_TYPE=mysqli ||
| POSTFIXADMIN_DB_HOST=db ||
| POSTFIXADMIN_SMTP_SERVER=postfix ||
| POSTFIXADMIN_SMTP_PORT=25 ||
| POSTFIXADMIN_ENCRYPT=md5crypt ||
| POSTFIXADMIN_DB_USER=postfix ||
| POSTFIXADMIN_DB_NAME=postfix ||
| POSTFIXADMIN_DB_PASSWORD=\_PASS\_ | # Value of the MYSQL PASSWORD from ./setup.sh |
| PA_SETUP_PASS=\_PASS\_ | # Value of the MYSQL PASSWORD from `./setup.sh` |
| MYSQL_USER=postfix ||
| MYSQL_DATABASE=postfix ||
| MYSQL_ROOT_PASSWORD=\_PASS\_ | # Value of the MYSQL PASSWORD from `./setup.sh` |
| MYSQL_PASSWORD=\_PASS\_ | # Value of the MYSQL PASSWORD from `./setup.sh` |
| CONTROLLER_PASSWORD=\_RSPAMD\_ | # Value of the RSPAMD web access password from `./setup.sh` |
| DOMAIN=\_DOMAIN\_ | # Value of the mail domain from `./setup.sh` |
| MAILNAME=\_MAILNAME\_ | # Value of the FQDN hostname from `./setup.sh` |
| POSTMASTER=admin@\_DOMAIN\_ | # Value of the mail domain from `./setup.sh` |
| RELAYHOST=false | # set to false so as to not use a relay host. Specify an smtp server, if all mail is to be sent to a remote host |
| FILTER_MIME=false | # Toggle of file extension checking in Postfix. Additionally edit `docker-mail-server/postfix/rootfs/etc/postfix/mime_header_checks` |
| FILTER_VIRUS=true | # Toggle virus checking |
| ENABLE_IMAP=true | # Toggle of `IMAP` |
| ENABLE_POP3=true | # Toggle of `POP3` |
| CONTROLLER_SECURE_NETWORK=172.16.0.0/12 | |
| RECIPIENT_DELIMITER=- | # `Postfix recipient_delimiter` parameter. Lets mail to be sent at `user-anything@example.com`. Mail is then delivered at `user@example.com`. |
| TZ=Europe/Moscow | # Timezone, if set it will be written to `/etc/timezone` inside the container |
| LETSENCRYPT=true | # Toggle of automatic LETSENCRYPT certificate renewal |
| LE_EMAIL=admin@\_DOMAIN\_ | # Value of the mail domain from `./setup.sh` |
| LE_FQDN=\_MAILNAME\_ | # Value of the FQDN hostname from `./setup.sh` |
| LDAP_PASS=\_LDAP\_ | # Value of the LDAP user admin password from `./setup.sh` |
| ADDRESSBOOK_PASS=\_ADDRESSBOOKPASS\_ | # Value of the LDAP user adressbook password from `./setup.sh` |
| ROUNDCUBEMAIL_DB_TYPE=mysql |
| ROUNDCUBEMAIL_DB_HOST=db |
| ROUNDCUBEMAIL_DB_NAME=roundcube |
| ROUNDCUBEMAIL_DB_USER=postfix |
| ROUNDCUBEMAIL_DB_PASSWORD=\_PASS\_ | # Value of the MYSQL PASSWORD from `./setup.sh` |
| ROUNDCUBEMAIL_SMTP_SERVER=tls://\_MAILNAME\_ | # Value of the FQDN hostname from `./setup.sh` |
| ROUNDCUBEMAIL_DEFAULT_HOST=tls://\_MAILNAME\_ | # Value of the FQDN hostname from `./setup.sh` |
| ROUNDCUBEMAIL_UPLOAD_MAX_FILESIZE=20M ||
| ROUNDCUBEMAIL_PLUGINS=archive,zipdownload||
| ROUNDCUBEMAIL_REQUEST_PATH=/mail/||


## Detached containers used in the project:

| Container | Description | Source | Ports |
| ------ | ------ | ------ | ------ |
| db | Postfixadmin and Roundcube database | mariadb server | port 3306(no outside access) |
| postfixadmin | Postfix mail server web administration interface | postfixadmin server | port 443, https://mail.example.org/postfixadmin/login.php |
| nginx | Proxy server with LetsEncrypt support, proxy for postfixadmin (IP-restricted access), proxy for Roundcube (web mail client), proxy for RSPAMD web interface (IP-restricted access) | container used can be found under umputun/nginx-le:latest | port 80 for LetsEncrypt domain authentication, port 443 proxy for postfixadmin, Roundcube, RSPAMD |
| postfix | smtp server | build: postfix | port 25 only |
| dovecot | IMAP, POP3, Submission, LMTP, Sieve server | build: dovecot | imap 110, imaps 143(STARTTLS), submission 587(STARTTLS), imaps 993(SSL), pop3s 995, lmtp port 2003 (no outside access), managesieve 4190 (STARTTLS) |
| filter | Rspamd server | build: filter | port 11332 communicating with Postfix(no outside access), Web statistics server port 11334, from outside the network use https://mail.example.org/rspamd/ |
| redis | Database server redis for Rspamd | redis:alpine | port 6379 (no outside access) |
| virus | Antivirus server Clamav | build: virus | port 3310 communicating with Rspamd(no outside accesss) |
| roundcube | Email web client | roundcube/roundcubemail:latest | outside port 443, use https://mail.example.org/mail/ |
| openldap | Openldap сервер | build: openldap | port 636 |
| restarter | Cron used to restart openldap, postfix, dovecot containers on a daily basis (utilized to keep LetsEncrypt certificates updated) | image: docker | |

## HOWTO

### Roundcube mail web interface (additional features).

**Open the mail web interface https://mail.example.org/mail/.**

1. Sieve filters.
Open `Settings`, `Filters`. On `Filters` tab click `+`. A filter creation window will appear. Countless customization options are possible regarding mail conditioning e.g. sorting into different folders, forwarding, or automatically sending an email back informing addressee is on leave. Click `save` when filter creation is complete.

2. LDAP address book. 
Click `Contacts`. An LDAP address book will appear shortly. It is updated on an hourly basis. Contacts consist of `users` of all of Your domains as well as `To every user of the domain` aliases.

### Setting up Thunderbird

**Server settings**

> - Server type: **IMAP mail server** 
> - Server name: `mail.example.org` Port: `143`
> - Username: `admin@example.org`
> - Security protocol: `STARTTLS`
> - Authentication method: `Plain password`

**Outbound mail server**

> - Server type: **SMTP-server** 
> - Server name: `mail.example.org` Port: `587`
> - Username: `admin@example.org`
> - Security protocol: `STARTTLS`
> - Authentication method: `Plain password`


**Address book** (File, create, LDAP catalog/dictionary)

> **_Main tab_**
> - Name: `mail.example.org`
> - Server name: `mail.example.org`
> - Root element name (Base DN): `ou=addressbook,dc=mail,dc=example,dc=org`
> - Port: `636`
> - User name (Bind DN): `cn=addressbook,dc=mail,dc=example,dc=org` (you will be asked to enter a password - this is the value of the `ADDRESS BOOK_PASS` variable of the `.env` file)
> - `X` Use secure connection (SSL)
> 
> **_Extras tab_**
> 
> - Do not return more than `1000` results
> - Search scope `Subtree`
> - Search filter `(mail=*)`
> - Authentication method `plain`

### Sieve – filter rules to be used by Dovecot.

1. Email marked as Spam is put into the Spam folder.
2. If user puts an email into the Spam folder then the Spam filter will scan through that email contents in order to learn identifying spam in an according manner.
3. Oppositely, if user moves an email from the Spam folder elsewhere then the Spam filter will learn to identify this email pattern as regular mail.

### How to enable filetype check on Postfix level?

1. Edit the `docker-mail-server/postfix/rootfs/etc/postfix/mime_header_checks` file
`/name=[^>]*\.(bat|com|exe|dll|vbs|docm|doc|dzip)/ REJECT`
2. In the`.env.dist` file use this variable-value: `FILTER_MIME=true`
3. Recreate the `.env` file by running ./setup.sh, or edit the `FILTER_MIME=true` variable-value in the `.env` file.
4. You will need to recreate the postfix container.

### RECIPIENT_DELIMITER

`RECIPIENT_DELIMITER=-` is the default value in the .env file
It lets You send email towards `user-enythingHere@example.com` from the outside. Email will be delivered to `user@example.com`.

### How to create backups without the db container?

From inside the docker-mail-server directory run
`source .env && docker-compose exec db mysqldump -uroot -p${MYSQL_ROOT_PASSWORD} --all-databases > db-dump-$(date +%F_%H-%M-%S).sql`

### How to restore database from an archive?

`source .env && docker-compose exec -T db mysql -uroot -p${MYSQL_ROOT_PASSWORD} < mariadb-dump.sql`

### IMAPSync utility

Aids in transferring mailboxes from one email server to another. Works on `Ubuntu 20.04`. Source: https://www.ylsoftware.com/news/733

> Utility shall be launched on a separate host.

1. Installing dependencies.

```
sudo apt install  \
libauthen-ntlm-perl     \
libclass-load-perl      \
libcrypt-ssleay-perl    \
libdata-uniqid-perl     \
libdigest-hmac-perl     \
libdist-checkconflicts-perl \
libencode-imaputf7-perl     \
libfile-copy-recursive-perl \
libfile-tail-perl       \
libio-compress-perl     \
libio-socket-inet6-perl \
libio-socket-ssl-perl   \
libio-tee-perl          \
libmail-imapclient-perl \
libmodule-scandeps-perl \
libnet-dbus-perl        \
libnet-ssleay-perl      \
libpar-packer-perl      \
libreadonly-perl        \
libregexp-common-perl   \
libsys-meminfo-perl     \
libterm-readkey-perl    \
libtest-fatal-perl      \
libtest-mock-guard-perl \
libtest-mockobject-perl \
libtest-pod-perl        \
libtest-requires-perl   \
libtest-simple-perl     \
libunicode-string-perl  \
liburi-perl             \
libtest-nowarnings-perl \
libtest-deep-perl       \
libtest-warn-perl       \
make                    \
cpanminus               \
```
2. Downloading imapsync `git clone https://github.com/imapsync/imapsync.git`
3. `cd imapsync`
4. Create a file named `mails.csv`

`old_login|old_pass|new_login|new_pass|`

The login:password combination of the older server are stored in "old_login" and "old_pass" accordingly. Likewise, "new_login" and "new_pass" depict the new login:password.

5. Create a file named `sync.sh`
6. Make it runnable `chmod +x sync.sh`
7. sync.sh shall contain those line:

```
#!/bin/bash

cd `dirname $0`

for line in `cat mails.csv | grep -v ^#`; do
        M_USER=`echo ${line} | cut -d '|' -f1`
        M_PASS=`echo ${line} | cut -d '|' -f2`
        N_USER=`echo ${line} | cut -d '|' -f3`
        N_PASS=`echo ${line} | cut -d '|' -f4`
        echo "Processing ${M_USER}..."
        ./imapsync --host1 imap.yandex.com --port1 993 --user1 ${M_USER} --password1 ${M_PASS} --ssl1 --host2 mail.example.org --port2 143 --user2 ${N_USER} --password2 ${N_PASS} --tls2
        if [ $? -ne "0" ]; then
                echo ${M_USER} >> mail_errors
        fi
done
```
8. Upon completion of the script execution "mail_errors" shall contain all untransfered users.

### In case something went wrong.

- From inside the docker-mail-server directory run ```docker-compose down```
- After that, run ```docker system prune -a --volumes```. It will perform a cleanup of all halted containers and unused networks
- Fix the errors and run ```docker-compose up -d```
- Recreating users is unnecessary. Confirming the password with a procedure listed below is enough.
- Open https://mail.example.org/postfixadmin/setup.php (```mail.example.org``` shall be changed to Your server name)
- Enter the mysql password
- To completely wipe all data delete /mail and docker-mail-server directories
- Logs can be accessed by running ```docker-compose logs -f```

### Service Domain Users

* When creating a new domain, the aliases `abuse@newdomain`, `hostmaster@newdomain`, `postmaster@newdomain`, `webmaster@newdomain` are automatically created in `PostfixAdmin`. They automatically forward mail to the main domain, specifically `admin@example.org` mailbox.


## Thanks



## See also

- This project is based on the following instruction [ISPMail guide](https://workaround.org/ispmail/) and the following project [Docker-mailserver](https://github.com/jeboehm/docker-mailserver). 

- An automatic receipt and renewal of **LetsEncript** certificate is implemented based on the following project [NGINX-LE - Nginx web and proxy with automatic let's encrypt](https://github.com/nginx-le/nginx-le).
 

## License

MIT
