#!/bin/sh
set -e

RSPAMD_HOST=filter
MDA_HOST=dovecot
MYSQL_HOST=db
SSL_CERT=/media/tls/le-crt.pem
SSL_KEY=/media/tls/le-key.pem
MYNETWORKS=127.0.0.0/8\ 10.0.0.0/8\ 172.16.0.0/12\ 192.168.0.0/16

postconf virtual_mailbox_domains="proxy:mysql:/etc/postfix/mysql_virtual_domains_maps.cf"
postconf virtual_mailbox_maps="proxy:mysql:/etc/postfix/mysql_virtual_mailbox_maps.cf"
postconf virtual_alias_maps="proxy:mysql:/etc/postfix/mysql_virtual_alias_maps.cf"
postconf smtpd_milters="inet:${RSPAMD_HOST}:11332"
postconf non_smtpd_milters="inet:${RSPAMD_HOST}:11332"
postconf milter_protocol=6
postconf milter_mail_macros="i {mail_addr} {client_addr} {client_name} {auth_authen}"
postconf milter_default_action=accept
postconf virtual_transport="lmtp:${MDA_HOST}:2003"
postconf smtp_tls_security_level=may
postconf smtpd_tls_security_level=may
postconf smtpd_tls_auth_only=yes
postconf smtpd_tls_cert_file="${SSL_CERT}"
postconf smtpd_tls_key_file="${SSL_KEY}"
postconf smtpd_discard_ehlo_keywords="silent-discard, dsn"
postconf soft_bounce=no
postconf message_size_limit=52428800
postconf mailbox_size_limit=0
postconf maximal_queue_lifetime=1h
postconf bounce_queue_lifetime=1h
postconf maximal_backoff_time=15m
postconf minimal_backoff_time=5m
postconf queue_run_delay=5m
postconf myhostname="${MAILNAME}"
postconf mynetworks="${MYNETWORKS}"
postconf recipient_delimiter="${RECIPIENT_DELIMITER}"
postconf maillog_file="/dev/stdout"

if [ "${FILTER_MIME}" == "true" ]
then
  postconf mime_header_checks=regexp:/etc/postfix/mime_header_checks
fi

if [ "${RELAYHOST}" != "false" ]
then
  postconf relayhost=${RELAYHOST}
fi

sed -i "s|_MYSQL_USER_|${MARIADB_USER}|g" /etc/postfix/mysql_virtual_alias_maps.cf
sed -i "s|_MYSQL_PASSWORD_|${MARIADB_PASSWORD}|g" /etc/postfix/mysql_virtual_alias_maps.cf
sed -i "s|_MYSQL_HOST_|${MYSQL_HOST}|g" /etc/postfix/mysql_virtual_alias_maps.cf
sed -i "s|_MYSQL_DATABASE_|${MARIADB_DATABASE}|g" /etc/postfix/mysql_virtual_alias_maps.cf

sed -i "s|_MYSQL_USER_|${MARIADB_USER}|g" /etc/postfix/mysql_virtual_domains_maps.cf
sed -i "s|_MYSQL_PASSWORD_|${MARIADB_PASSWORD}|g" /etc/postfix/mysql_virtual_domains_maps.cf
sed -i "s|_MYSQL_HOST_|${MYSQL_HOST}|g" /etc/postfix/mysql_virtual_domains_maps.cf
sed -i "s|_MYSQL_DATABASE_|${MARIADB_DATABASE}|g" /etc/postfix/mysql_virtual_domains_maps.cf

sed -i "s|_MYSQL_USER_|${MARIADB_USER}|g" /etc/postfix/mysql_virtual_mailbox_maps.cf
sed -i "s|_MYSQL_PASSWORD_|${MARIADB_PASSWORD}|g" /etc/postfix/mysql_virtual_mailbox_maps.cf
sed -i "s|_MYSQL_HOST_|${MYSQL_HOST}|g" /etc/postfix/mysql_virtual_mailbox_maps.cf
sed -i "s|_MYSQL_DATABASE_|${MARIADB_DATABASE}|g" /etc/postfix/mysql_virtual_mailbox_maps.cf



while [ ! -e ${SSL_CERT} ]
do
echo "wait file sert"
sleep 5
done

/usr/libexec/postfix/master -d
