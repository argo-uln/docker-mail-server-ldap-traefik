#!/bin/bash

#set -ex

ulimit -n 1024

DEBUG_LEVEL=256

while [ ! -e /media/tls/le-crt.pem ]
do
echo "wait file sert"
sleep 5
done

#setfacl -m "u:openldap:r" /media/tls/{dhparams,le-chain-crt,le-crt,le-key}.pem
setfacl -m "u:openldap:r" /media/tls/{le-chain-crt,le-crt,le-key}.pem

echo "127.0.0.1 ${MAILNAME}" >> /etc/hosts

if [ ! -e /root/docker_bootstrapped ]; then
	touch /root/docker_bootstrapped

	/usr/sbin/slapd -h "ldap:/// ldaps:/// ldapi:///" -u openldap -d ${DEBUG_LEVEL} &
	slapd_pid=$!
	sleep 3


	ldapmodify -Y EXTERNAL -H ldapi:/// -f /etc/ldap/ldap-tls.ldif


	#I will transform the host name into BASEDN.
	IFS=$'.'
	for n in $MAILNAME
	do
	  BASEDN+="dc=$n,"
	done
	BASEDN=${BASEDN%?}
	ROOTDN="cn=admin,$BASEDN"

	ldapmodify -a -x -D "$ROOTDN" -w $LDAP_PASS -H ldapi:/// -f /etc/ldap/ldap-add-ou-addressbook.ldif
	ADDRESSBOOK_PASS_HASH="$(/sbin/slappasswd -h '{SSHA}' -s $ADDRESSBOOK_PASS)"
	sed -i "s|_ADDRESSBOOK_PASS_|$ADDRESSBOOK_PASS_HASH|g" /etc/ldap/addressbook.ldif
	ldapmodify -a -x -D "$ROOTDN" -w $LDAP_PASS -H ldapi:/// -f /etc/ldap/addressbook.ldif
	ldapadd -Y EXTERNAL -H ldapi:/// -f /etc/ldap/oclAccess.ldif

	kill "$slapd_pid"
	wait "$slapd_pid"
fi

(
 echo "Add everyone alias for all domain to ldap and mysql database"
 while :; do
    echo "trying to update ldap and mysql ..."
    /root/mysql_ldap.php
    sleep 3600s
 done
) &

exec /usr/sbin/slapd -h "ldap:/// ldaps:/// ldapi:///" -u openldap -d $DEBUG_LEVEL
