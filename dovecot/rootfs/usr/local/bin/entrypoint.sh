#!/bin/sh

set -ex

MYSQL_HOST=db
SUBMISSION_HOST=postfix
SSL_CERT=/media/tls/le-crt.pem
SSL_KEY=/media/tls/le-key.pem

sed -i "s|_MYSQL_USER_|${MARIADB_USER}|g" /etc/dovecot/dovecot-sql.conf.ext
sed -i "s|_MYSQL_PASSWORD_|${MARIADB_PASSWORD}|g" /etc/dovecot/dovecot-sql.conf.ext
sed -i "s|_MYSQL_HOST_|${MYSQL_HOST}|g" /etc/dovecot/dovecot-sql.conf.ext
sed -i "s|_MYSQL_DATABASE_|${MARIADB_DATABASE}|g" /etc/dovecot/dovecot-sql.conf.ext

sed -i "s|_POSTMASTER_|${POSTMASTER}|g" /etc/dovecot/conf.d/15-lda.conf
sed -i "s|_MAILNAME_|${MAILNAME}|g" /etc/dovecot/conf.d/15-lda.conf
sed -i "s|_SUBMISSION_HOST_|${SUBMISSION_HOST}|g" /etc/dovecot/conf.d/15-lda.conf
sed -i "s|_RECIPIENT_DELIMITER_|${RECIPIENT_DELIMITER}|g" /etc/dovecot/conf.d/15-lda.conf

sed -i "s|_SUBMISSION_HOST_|${SUBMISSION_HOST}|g" /etc/dovecot/conf.d/20-submission.conf

if [ ! -e /etc/dovecot/docker_bootstrapped ]; then
 	touch /etc/dovecot/docker_bootstrapped

if [ "${ENABLE_IMAP}" == "true" ]; then
cat >> /etc/dovecot/conf.d/10-master.conf <<EOF
service imap-login {
  inet_listener imap {
    #port = 143
  }
  inet_listener imaps {
    #port = 993
    #ssl = yes
  }
}
service imap {
}
protocols = \$protocols imap
EOF
fi
if [ "${ENABLE_POP3}" == "true" ]; then
cat >> /etc/dovecot/conf.d/10-master.conf <<EOF
service pop3-login {
  inet_listener pop3 {
    #port = 110
  }
  inet_listener pop3s {
    #port = 995
    #ssl = yes
  }
}
service pop3 {
}
protocols = \$protocols pop3
EOF
fi

fi

while [ ! -e ${SSL_CERT} ]; do
	echo "wait file sert"
	sleep 5
done

/usr/sbin/dovecot -F
