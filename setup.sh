#!/bin/bash

if ! [ -d /mail ]; then
         mkdir /mail
         chmod 777 /mail
fi

chmod 777 filter/dkim

# Checking  php-cli
if [ ! -e '/usr/bin/php' ]; then
     apt -y install php-cli
     if [ $? -ne 0 ]; then
         echo "Error: Can't install php-cli"
         exit $1
     fi
fi

read -p "Please enter MYSQL PASSWORD: " PASS
read -p "Please enter FQDN hostname: " MAILNAME
read -p "Please enter mail domain: " DOMAIN
read -p "Please enter RSPAMD web access password: " RSPAMD_PASS
read -p "Please enter LDAP user admin password: " LDAP_ADMIN_PASS
read -p "Please enter LDAP user adressbook password: " ADDRESSBOOKPASS
echo "Please enter the ip addresses separated by a comma to access the web interfase services traefik, postfixadmin, rspamd. Default, access to them is closed"
read -p "xxx.xxx.xxx.xxx/32, yyy.yyy.yyy.yyy/32: " ip_allow

cp $PWD/.env.dist $PWD/.env
sed -i "s|_PASS_|$PASS|g" $PWD/.env
sed -i "s|_RSPAMD_|$RSPAMD_PASS|g" $PWD/.env
sed -i "s|_DOMAIN_|$DOMAIN|g" $PWD/.env
sed -i "s|_MAILNAME_|$MAILNAME|g" $PWD/.env
sed -i "s|_LDAP_|$LDAP_ADMIN_PASS|g" $PWD/.env
sed -i "s|_ADDRESSBOOKPASS_|$ADDRESSBOOKPASS|g" $PWD/.env
sed -i "s|_IPALLOW_|$ip_allow|g" $PWD/.env

printf "$LDAP_ADMIN_PASS" > $PWD/openldap/ldap.pass

#I will transform the host name into BASEDN.
IFS=$'.'
for n in $MAILNAME
do
 BASEDN+="dc=$n,"
done
BASEDN=${BASEDN%?}
ROOTDN="cn=admin,$BASEDN"

PA_SETUP_HASH="$(/usr/bin/php -r 'echo password_hash("'$PASS'", PASSWORD_DEFAULT);')"

set -o allexport
source .env
set +o allexport

cp $PWD/openldap/ldif/addressbook.ldif.dist $PWD/openldap/ldif/addressbook.ldif
sed -i "s|_BASEDN_|$BASEDN|g" $PWD/openldap/ldif/addressbook.ldif

cp $PWD/openldap/ldif/ldap-add-ou-addressbook.ldif.dist $PWD/openldap/ldif/ldap-add-ou-addressbook.ldif
sed -i "s|_BASEDN_|$BASEDN|g" $PWD/openldap/ldif/ldap-add-ou-addressbook.ldif

cp $PWD/openldap/ldif/oclAccess.ldif.dist $PWD/openldap/ldif/oclAccess.ldif
sed -i "s|_BASEDN_|$BASEDN|g" $PWD/openldap/ldif/oclAccess.ldif

cp $PWD/openldap/conf/mysql_ldap.php.dist $PWD/openldap/conf/mysql_ldap.php
sed -i "s|_ROOTDN_|$ROOTDN|g" $PWD/openldap/conf/mysql_ldap.php
sed -i "s|_LDAP_|$LDAP_ADMIN_PASS|g" $PWD/openldap/conf/mysql_ldap.php
sed -i "s|_POSTFIX_PASS_|$PASS|g" $PWD/openldap/conf/mysql_ldap.php
sed -i "s|_BASEDN_|$BASEDN|g" $PWD/openldap/conf/mysql_ldap.php
chmod +x $PWD/openldap/conf/mysql_ldap.php

cp $PWD/roundcube/config/ldap.php.dist $PWD/roundcube/config/ldap.php
sed -i "s|_BASEDN_|$BASEDN|g" $PWD/roundcube/config/ldap.php
sed -i "s|_ROOTDN_|$ROOTDN|g" $PWD/roundcube/config/ldap.php
sed -i "s|_LDAP_|$LDAP_ADMIN_PASS|g" $PWD/roundcube/config/ldap.php

cp $PWD/roundcube/config/managesieve.php.dist $PWD/roundcube/config/managesieve.php
sed -i "s|_MAILNAME_|$MAILNAME|g" $PWD/roundcube/config/managesieve.php

cp $PWD/postfixadmin/config.local.php.dist $PWD/postfixadmin/config.local.php
sed -i "s|_POSTFIXADMIN_DB_TYPE_|$POSTFIXADMIN_DB_TYPE|g" $PWD/postfixadmin/config.local.php
sed -i "s|_POSTFIXADMIN_DB_HOST_|$POSTFIXADMIN_DB_HOST|g" $PWD/postfixadmin/config.local.php
sed -i "s|_POSTFIXADMIN_DB_USER_|$POSTFIXADMIN_DB_USER|g" $PWD/postfixadmin/config.local.php
sed -i "s|_POSTFIXADMIN_DB_PASSWORD_|$PASS|g" $PWD/postfixadmin/config.local.php
sed -i "s|_POSTFIXADMIN_DB_NAME_|$POSTFIXADMIN_DB_NAME|g" $PWD/postfixadmin/config.local.php
sed -i "s|_POSTFIXADMIN_SMTP_SERVER_|$POSTFIXADMIN_SMTP_SERVER|g" $PWD/postfixadmin/config.local.php
sed -i "s|_PA_SETUP_HASH_|$PA_SETUP_HASH|g" $PWD/postfixadmin/config.local.php
sed -i "s|_DOMAIN_|$DOMAIN|g" $PWD/postfixadmin/config.local.php

cp $PWD/init/roundcube.sql.dist $PWD/init/roundcube.sql
sed -i "s|_ROUNDCUBEMAIL_DB_NAME_|$ROUNDCUBEMAIL_DB_NAME|g" $PWD/init/roundcube.sql
sed -i "s|_ROUNDCUBEMAIL_DB_USER_|$ROUNDCUBEMAIL_DB_USER|g" $PWD/init/roundcube.sql


echo "You can start the build and run with the command:"
echo "docker compose up -d"

